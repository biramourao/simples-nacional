import {
  Component,
  OnInit
} from '@angular/core';
import {
  NgModel
} from '@angular/forms';
import {
  FormsModule
} from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatTableDataSource} from '@angular/material/table';
/** @title Simple form field */

// Interface Tabela de Resultado do Calculo
export interface TabelaResultadoDoCalculo {
  aliquotaNominal: number;
  fatorR: number;
  aliquotaEfetiva: number;
  imposto: number;
}

// Interface Tabela de Distribuição de impostos I
export interface TabelaDistribuicaoDeImpopstos1 {
  irpj: number;
  csll: number;
  cofins: number;
  pis: number;
}

// Interface Tabela de Distribuição de impostos II
export interface TabelaDistribuicaoDeImpopstos2 {
  cpp: number;
  ipi: number;
  icms: number;
  iss: number;
}

@Component({
  selector: 'app-calculo',
  templateUrl: './calculo.component.html',
  styleUrls: ['./calculo.component.css']
})

export class CalculoComponent implements OnInit {

  rbt12 = 0;
  faturamentoMes = 0;
  fp12 = 0;
  aliquotaNominal = 0;
  aliquotaEfetiva = 0;
  fatorR = 0;
  irpj = 0;
  csll = 0;
  cofins = 0;
  pis = 0;
  cpp = 0;
  icms = 0;
  iss = 0;
  ipi = 0;
  impostoDas = 0;
  parcelaADeduzir = 0;
  anexo = [{
    id: 1,
    name: 'Anexo I'
  },
  {
    id: 2,
    name: 'Anexo II'
  },
  {
    id: 3,
    name: 'Anexo III'
  },
  {
    id: 4,
    name: 'Anexo IV'
  },
  {
    id: 5,
    name: 'Anexo V'
  }
  ];
  anexoSelecionado;
  st = false;
  // Informações da Tabela de Resultado do Calculo
  // Modelo de dados
  modeloTabelaDeResultadoDoCalculo: TabelaResultadoDoCalculo[] = [
    {aliquotaEfetiva: this.aliquotaEfetiva, fatorR: this.fatorR, aliquotaNominal: this.aliquotaNominal, imposto: this.impostoDas}
  ];
  // Coluna da Tabela
  tabelaResultadoDoCalculoColunas = ['aliquotaNominal', 'fatorR', 'aliquotaEfetiva', 'totalImposto'];
  // Atribuição dos dados a tabela;
  tabelaResultadoDoCalculoDados = new MatTableDataSource(this.modeloTabelaDeResultadoDoCalculo);

  // Informações da Tabela de Distribuição de impostos I
  // Modelo de dados
  modeloTabelaDistribuicaoDeImpopstos1: TabelaDistribuicaoDeImpopstos1[] = [
    {irpj: this.irpj, csll: this.csll, pis: this.pis, cofins: this.cofins}
  ];
  // Coluna da Tabela
  tabelaDistribuicaoDeImpopstos1Colunas = ['IRPJ', 'CSLL', 'PIS', 'COFINS'];
  // Atribuição dos dados a tabela;
  tabelaDistribuicaoDeImpopstos1Dados = new MatTableDataSource(this.modeloTabelaDistribuicaoDeImpopstos1);

  // Informações da Tabela de Distribuição de impostos I
  // Modelo de dados
  modeloTabelaDistribuicaoDeImpopstos2: TabelaDistribuicaoDeImpopstos2[] = [
    {cpp: this.cpp, ipi: this.ipi, icms: this.icms, iss: this.iss}
  ];
  // Coluna da Tabela
  tabelaDistribuicaoDeImpopstos2Colunas = ['CPP', 'IPI', 'ICMS', 'ISS'];
  // Atribuição dos dados a tabela;
  tabelaDistribuicaoDeImpopstos2Dados = new MatTableDataSource(this.modeloTabelaDistribuicaoDeImpopstos2);

  constructor() {}
  calcularImposto(anexoSelecionado) {
    this.aliquotaNominal = 0;
    this.aliquotaEfetiva = 0;
    this.fatorR = 0;
    this.irpj = 0;
    this.csll = 0;
    this.cofins = 0;
    this.pis = 0;
    this.cpp = 0;
    this.icms = 0;
    this.ipi = 0;
    this.iss = 0;
    this.impostoDas = 0;
    this.parcelaADeduzir = 0;
    switch (anexoSelecionado) {
      case '1':
        {
          if (this.rbt12 >= 0 && this.rbt12 <= 180000) {
            this.aliquotaNominal = 0.04;
            this.parcelaADeduzir = 0;
            if (this.rbt12 === 0 || this.rbt12 === undefined) {
              this.aliquotaEfetiva = this.aliquotaNominal;
            } else {
              this.aliquotaEfetiva = (((this.rbt12 * this.aliquotaNominal) - this.parcelaADeduzir) / this.rbt12);
            }
            this.impostoDas = this.faturamentoMes * this.aliquotaEfetiva;
            this.irpj = this.impostoDas * 0.055;
            this.csll = this.impostoDas * 0.035;
            this.cofins = this.impostoDas * 0.1274;
            this.pis = this.impostoDas * 0.0276;
            this.cpp = this.impostoDas * 0.415;
            this.icms = this.impostoDas * 0.34;
          } else if (this.rbt12 >= 180000.01 && this.rbt12 <= 360000) {
            this.aliquotaNominal = 0.073;
            this.parcelaADeduzir = 5940;
            this.aliquotaEfetiva = (((this.rbt12 * this.aliquotaNominal) - this.parcelaADeduzir) / this.rbt12);
            this.impostoDas = this.faturamentoMes * this.aliquotaEfetiva;
            this.irpj = this.impostoDas * 0.055;
            this.csll = this.impostoDas * 0.035;
            this.cofins = this.impostoDas * 0.1274;
            this.pis = this.impostoDas * 0.0276;
            this.cpp = this.impostoDas * 0.415;
            this.icms = this.impostoDas * 0.34;

          } else if (this.rbt12 >= 360000.01 && this.rbt12 <= 720000) {
            this.aliquotaNominal = 0.095;
            this.parcelaADeduzir = 13860;
            this.aliquotaEfetiva = (((this.rbt12 * this.aliquotaNominal) - this.parcelaADeduzir) / this.rbt12);
            this.impostoDas = this.faturamentoMes * this.aliquotaEfetiva;
            this.irpj = this.impostoDas * 0.055;
            this.csll = this.impostoDas * 0.035;
            this.cofins = this.impostoDas * 0.1274;
            this.pis = this.impostoDas * 0.0276;
            this.cpp = this.impostoDas * 0.42;
            this.icms = this.impostoDas * 0.335;

          } else if (this.rbt12 >= 720000.01 && this.rbt12 <= 1800000) {
            this.aliquotaNominal = 0.107;
            this.parcelaADeduzir = 22500;
            this.aliquotaEfetiva = (((this.rbt12 * this.aliquotaNominal) - this.parcelaADeduzir) / this.rbt12);
            this.impostoDas = this.faturamentoMes * this.aliquotaEfetiva;
            this.irpj = this.impostoDas * 0.055;
            this.csll = this.impostoDas * 0.035;
            this.cofins = this.impostoDas * 0.1274;
            this.pis = this.impostoDas * 0.0276;
            this.cpp = this.impostoDas * 0.42;
            this.icms = this.impostoDas * 0.335;

          } else if (this.rbt12 >= 1800000.01 && this.rbt12 <= 3600000) {
            this.aliquotaNominal = 0.143;
            this.parcelaADeduzir = 87300;
            this.aliquotaEfetiva = (((this.rbt12 * this.aliquotaNominal) - this.parcelaADeduzir) / this.rbt12);
            this.impostoDas = this.faturamentoMes * this.aliquotaEfetiva;
            this.irpj = this.impostoDas * 0.055;
            this.csll = this.impostoDas * 0.035;
            this.cofins = this.impostoDas * 0.1274;
            this.pis = this.impostoDas * 0.0276;
            this.cpp = this.impostoDas * 0.42;
            this.icms = this.impostoDas * 0.335;

          } else if (this.rbt12 >= 3600000.01 && this.rbt12 <= 4800000) {
            this.aliquotaNominal = 0.19;
            this.parcelaADeduzir = 378000;
            this.aliquotaEfetiva = (((this.rbt12 * this.aliquotaNominal) - this.parcelaADeduzir) / this.rbt12);
            this.impostoDas = this.faturamentoMes * this.aliquotaEfetiva;
            this.irpj = this.impostoDas * 0.135;
            this.csll = this.impostoDas * 0.10;
            this.cofins = this.impostoDas * 0.2827;
            this.pis = this.impostoDas * 0.0613;
            this.cpp = this.impostoDas * 0.421;
            this.icms = 0;
          }

          if (this.st === true) {
            this.impostoDas = this.impostoDas - this.icms;
            this.icms = 0;
          }
          break;
        }
      case '2':
        {
          if (this.rbt12 >= 0 && this.rbt12 <= 180000) {
            this.aliquotaNominal = 0.045;
            this.parcelaADeduzir = 0;
            if (this.rbt12 === 0 || this.rbt12 === undefined) {
              this.aliquotaEfetiva = this.aliquotaNominal;
            } else {
              this.aliquotaEfetiva = (((this.rbt12 * this.aliquotaNominal) - this.parcelaADeduzir) / this.rbt12);
            }
            this.impostoDas = this.faturamentoMes * this.aliquotaEfetiva;
            this.irpj = this.impostoDas * 0.055;
            this.csll = this.impostoDas * 0.035;
            this.cofins = this.impostoDas * 0.1151;
            this.pis = this.impostoDas * 0.0249;
            this.cpp = this.impostoDas * 0.375;
            this.ipi = this.impostoDas * 0.075;
            this.icms = this.impostoDas * 0.32;

          } else if (this.rbt12 >= 180000.01 && this.rbt12 <= 360000) {
            this.aliquotaNominal = 0.078;
            this.parcelaADeduzir = 5940;
            this.aliquotaEfetiva = (((this.rbt12 * this.aliquotaNominal) - this.parcelaADeduzir) / this.rbt12);
            this.impostoDas = this.faturamentoMes * this.aliquotaEfetiva;
            this.irpj = this.impostoDas * 0.055;
            this.csll = this.impostoDas * 0.035;
            this.cofins = this.impostoDas * 0.1151;
            this.pis = this.impostoDas * 0.0249;
            this.cpp = this.impostoDas * 0.375;
            this.ipi = this.impostoDas * 0.075;
            this.icms = this.impostoDas * 0.32;

          } else if (this.rbt12 >= 360000.01 && this.rbt12 <= 720000) {
            this.aliquotaNominal = 0.10;
            this.parcelaADeduzir = 13860;
            this.aliquotaEfetiva = (((this.rbt12 * this.aliquotaNominal) - this.parcelaADeduzir) / this.rbt12);
            this.impostoDas = this.faturamentoMes * this.aliquotaEfetiva;
            this.irpj = this.impostoDas * 0.055;
            this.csll = this.impostoDas * 0.035;
            this.cofins = this.impostoDas * 0.1151;
            this.pis = this.impostoDas * 0.0249;
            this.cpp = this.impostoDas * 0.375;
            this.ipi = this.impostoDas * 0.075;
            this.icms = this.impostoDas * 0.32;

          } else if (this.rbt12 >= 720000.01 && this.rbt12 <= 1800000) {
            this.aliquotaNominal = 0.112;
            this.parcelaADeduzir = 22500;
            this.aliquotaEfetiva = (((this.rbt12 * this.aliquotaNominal) - this.parcelaADeduzir) / this.rbt12);
            this.impostoDas = this.faturamentoMes * this.aliquotaEfetiva;
            this.irpj = this.impostoDas * 0.055;
            this.csll = this.impostoDas * 0.035;
            this.cofins = this.impostoDas * 0.1151;
            this.pis = this.impostoDas * 0.0249;
            this.cpp = this.impostoDas * 0.375;
            this.ipi = this.impostoDas * 0.075;
            this.icms = this.impostoDas * 0.32;

          } else if (this.rbt12 >= 1800000.01 && this.rbt12 <= 3600000) {
            this.aliquotaNominal = 0.147;
            this.parcelaADeduzir = 85000;
            this.aliquotaEfetiva = (((this.rbt12 * this.aliquotaNominal) - this.parcelaADeduzir) / this.rbt12);
            this.impostoDas = this.faturamentoMes * this.aliquotaEfetiva;
            this.irpj = this.impostoDas * 0.055;
            this.csll = this.impostoDas * 0.035;
            this.cofins = this.impostoDas * 0.1151;
            this.pis = this.impostoDas * 0.0249;
            this.cpp = this.impostoDas * 0.375;
            this.ipi = this.impostoDas * 0.075;
            this.icms = this.impostoDas * 0.32;
          } else if (!(this.rbt12 >= 3600000.01 && this.rbt12 <= 4800000)) {
          } else {
            this.aliquotaNominal = 0.30;
            this.parcelaADeduzir = 720000;
            this.aliquotaEfetiva = (((this.rbt12 * this.aliquotaNominal) - this.parcelaADeduzir) / this.rbt12);
            this.impostoDas = this.faturamentoMes * this.aliquotaEfetiva;
            this.irpj = this.impostoDas * 0.085;
            this.csll = this.impostoDas * 0.075;
            this.cofins = this.impostoDas * 0.2096;
            this.pis = this.impostoDas * 0.0454;
            this.cpp = this.impostoDas * 0.235;
            this.ipi = this.impostoDas * 0.35;
            this.icms = 0;
          }
          if (this.st === true) {
            this.impostoDas = this.impostoDas - this.icms;
            this.icms = 0;
          }
          break;
        }
      case '3':
        {
          if (this.rbt12 >= 0 && this.rbt12 <= 180000) {
            this.aliquotaNominal = 0.06;
            this.parcelaADeduzir = 0;
            if (this.rbt12 === 0 || this.rbt12 === undefined) {
              this.aliquotaEfetiva = this.aliquotaNominal;
            } else {
              this.aliquotaEfetiva = (((this.rbt12 * this.aliquotaNominal) - this.parcelaADeduzir) / this.rbt12);
            }
            this.impostoDas = this.faturamentoMes * this.aliquotaEfetiva;
            this.irpj = this.impostoDas * 0.04;
            this.csll = this.impostoDas * 0.035;
            this.cofins = this.impostoDas * 0.1282;
            this.pis = this.impostoDas * 0.0278;
            this.cpp = this.impostoDas * 0.4340;
            this.iss = this.impostoDas * 0.3350;

          } else if (this.rbt12 >= 180000.01 && this.rbt12 <= 360000) {
            this.aliquotaNominal = 0.1120;
            this.parcelaADeduzir = 9360;
            this.aliquotaEfetiva = (((this.rbt12 * this.aliquotaNominal) - this.parcelaADeduzir) / this.rbt12);
            this.impostoDas = this.faturamentoMes * this.aliquotaEfetiva;
            this.irpj = this.impostoDas * 0.04;
            this.csll = this.impostoDas * 0.035;
            this.cofins = this.impostoDas * 0.1405;
            this.pis = this.impostoDas * 0.0305;
            this.cpp = this.impostoDas * 0.4340;
            this.iss = this.impostoDas * 0.3200;
          } else if (this.rbt12 >= 360000.01 && this.rbt12 <= 720000) {
            this.aliquotaNominal = 0.1350;
            this.parcelaADeduzir = 17640;
            this.aliquotaEfetiva = (((this.rbt12 * this.aliquotaNominal) - this.parcelaADeduzir) / this.rbt12);
            this.impostoDas = this.faturamentoMes * this.aliquotaEfetiva;
            this.irpj = this.impostoDas * 0.04;
            this.csll = this.impostoDas * 0.035;
            this.cofins = this.impostoDas * 0.1364;
            this.pis = this.impostoDas * 0.0296;
            this.cpp = this.impostoDas * 0.4340;
            this.iss = this.impostoDas * 0.3250;
          } else if (this.rbt12 >= 720000.01 && this.rbt12 <= 1800000) {
            this.aliquotaNominal = 0.16;
            this.parcelaADeduzir = 35640;
            this.aliquotaEfetiva = (((this.rbt12 * this.aliquotaNominal) - this.parcelaADeduzir) / this.rbt12);
            this.impostoDas = this.faturamentoMes * this.aliquotaEfetiva;
            this.irpj = this.impostoDas * 0.04;
            this.csll = this.impostoDas * 0.035;
            this.cofins = this.impostoDas * 0.1364;
            this.pis = this.impostoDas * 0.0296;
            this.cpp = this.impostoDas * 0.4340;
            this.iss = this.impostoDas * 0.3250;
          } else if (this.rbt12 >= 1800000.01 && this.rbt12 <= 3600000) {
            this.aliquotaNominal = 0.21;
            this.parcelaADeduzir = 125640;
            this.aliquotaEfetiva = (((this.rbt12 * this.aliquotaNominal) - this.parcelaADeduzir) / this.rbt12);
            this.impostoDas = this.faturamentoMes * this.aliquotaEfetiva;
            this.irpj = this.impostoDas * 0.04;
            this.csll = this.impostoDas * 0.035;
            this.cofins = this.impostoDas * 0.1282;
            this.pis = this.impostoDas * 0.0278;
            this.cpp = this.impostoDas * 0.4340;
            this.iss = this.impostoDas * 0.3350;
            if (this.aliquotaEfetiva > 0.1492537) {

              this.irpj = ((this.aliquotaEfetiva - 0.05) * 0.0602) * this.impostoDas;
              this.csll = ((this.aliquotaEfetiva - 0.05) * 0.0526) * this.impostoDas;
              this.cofins = ((this.aliquotaEfetiva - 0.05) * 0.1928) * this.impostoDas;
              this.pis = ((this.aliquotaEfetiva - 0.05) * 0.0418) * this.impostoDas;
              this.cpp = ((this.aliquotaEfetiva - 0.05) * 0.6526) * this.impostoDas;
              this.iss = 0.05 * this.impostoDas;
            }
          } else if (this.rbt12 >= 3600000.01 && this.rbt12 <= 4800000) {
            this.aliquotaNominal = 0.33;
            this.parcelaADeduzir = 648000;
            this.aliquotaEfetiva = (((this.rbt12 * this.aliquotaNominal) - this.parcelaADeduzir) / this.rbt12);
            this.impostoDas = this.faturamentoMes * this.aliquotaEfetiva;
            this.irpj = this.impostoDas * 0.35;
            this.csll = this.impostoDas * 0.15;
            this.cofins = this.impostoDas * 0.1603;
            this.pis = this.impostoDas * 0.0347;
            this.cpp = this.impostoDas * 0.3050;
            this.iss = 0;
          }

          if (this.st === true) {
            this.impostoDas = this.impostoDas - this.iss;
            this.iss = 0;
          }
          break;
        }
      case '4':
        {
          if (this.rbt12 >= 0 && this.rbt12 <= 180000) {
            this.aliquotaNominal = 0.045;
            this.parcelaADeduzir = 0;
            if (this.rbt12 === 0 || this.rbt12 === undefined) {
              this.aliquotaEfetiva = this.aliquotaNominal;
            } else {
              this.aliquotaEfetiva = (((this.rbt12 * this.aliquotaNominal) - this.parcelaADeduzir) / this.rbt12);
            }
            this.impostoDas = this.faturamentoMes * this.aliquotaEfetiva;
            this.irpj = this.impostoDas * 0.1880;
            this.csll = this.impostoDas * 0.1520;
            this.cofins = this.impostoDas * 0.1767;
            this.pis = this.impostoDas * 0.0383;
            this.iss = this.impostoDas * 0.4450;
          } else if (this.rbt12 >= 180000.01 && this.rbt12 <= 360000) {
            this.aliquotaNominal = 0.09;
            this.parcelaADeduzir = 8100;
            this.aliquotaEfetiva = (((this.rbt12 * this.aliquotaNominal) - this.parcelaADeduzir) / this.rbt12);
            this.impostoDas = this.faturamentoMes * this.aliquotaEfetiva;
            this.irpj = this.impostoDas * 0.1980;
            this.csll = this.impostoDas * 0.1520;
            this.cofins = this.impostoDas * 0.2055;
            this.pis = this.impostoDas * 0.0445;
            this.iss = this.impostoDas * 0.40;
          } else if (this.rbt12 >= 360000.01 && this.rbt12 <= 720000) {
            this.aliquotaNominal = 0.102;
            this.parcelaADeduzir = 12420;
            this.aliquotaEfetiva = (((this.rbt12 * this.aliquotaNominal) - this.parcelaADeduzir) / this.rbt12);
            this.impostoDas = this.faturamentoMes * this.aliquotaEfetiva;
            this.irpj = this.impostoDas * 0.2080;
            this.csll = this.impostoDas * 0.1520;
            this.cofins = this.impostoDas * 0.1973;
            this.pis = this.impostoDas * 0.0427;
            this.iss = this.impostoDas * 0.40;
          } else if (this.rbt12 >= 720000.01 && this.rbt12 <= 1800000) {
            this.aliquotaNominal = 0.14;
            this.parcelaADeduzir = 39780;
            this.aliquotaEfetiva = (((this.rbt12 * this.aliquotaNominal) - this.parcelaADeduzir) / this.rbt12);
            this.impostoDas = this.faturamentoMes * this.aliquotaEfetiva;
            // 4a Faixa  17,80%  19,20%  18,90%  4,10%   40,00%
            this.irpj = this.impostoDas * 0.1780;
            this.csll = this.impostoDas * 0.1920;
            this.cofins = this.impostoDas * 0.1890;
            this.pis = this.impostoDas * 0.0410;
            this.iss = this.impostoDas * 0.40;
          } else if (this.rbt12 >= 1800000.01 && this.rbt12 <= 3600000) {
            this.aliquotaNominal = 0.22;
            this.parcelaADeduzir = 183780;
            this.aliquotaEfetiva = (((this.rbt12 * this.aliquotaNominal) - this.parcelaADeduzir) / this.rbt12);
            this.impostoDas = this.faturamentoMes * this.aliquotaEfetiva;
            // 5a Faixa  18,80%  19,20%  18,08%  3,92%   40,00% ( * )
            this.irpj = this.impostoDas * 0.1880;
            this.csll = this.impostoDas * 0.1920;
            this.cofins = this.impostoDas * 0.1808;
            this.pis = this.impostoDas * 0.0392;
            this.iss = this.impostoDas * 0.40;

            if (this.aliquotaEfetiva > 0.1492537) {
              /* ( * ) O percentual efetivo máximo devido ao ISS será de 5%, transferindo-se a diferença, de forma proporcional,
               * aos tributos federais da mesma faixa de receita bruta anual. Sendo assim, na 5a faixa, quando a alíquota efetiva
               * for superior a 12,5%, a repartição será:
               * 5a Faixa   comalíquota efetiva superior a 12,5%
               *  IRPJ                CSLL                Cofins                PIS/Pasep               ISS
               *  (Alíquota efetiva – 5%) x 31,33%  (Alíquota efetiva – 5%) x 32,00%  (Alíquota efetiva – 5%) x 30,13%
               * (Alíquota efetiva – 5%) x 6,54%   Percentual de ISS fixo em 5%
               */
              this.irpj = ((this.aliquotaEfetiva - 0.05) * 0.3133) * this.impostoDas;
              this.csll = ((this.aliquotaEfetiva - 0.05) * 0.3200) * this.impostoDas;
              this.cofins = ((this.aliquotaEfetiva - 0.05) * 0.3013) * this.impostoDas;
              this.pis = ((this.aliquotaEfetiva - 0.05) * 0.0654) * this.impostoDas;
              this.iss = 0.05 * this.impostoDas;
            }
          } else if (this.rbt12 >= 3600000.01 && this.rbt12 <= 4800000) {
            this.aliquotaNominal = 0.33;
            this.parcelaADeduzir = 828000;
            this.aliquotaEfetiva = (((this.rbt12 * this.aliquotaNominal) - this.parcelaADeduzir) / this.rbt12);
            this.impostoDas = this.faturamentoMes * this.aliquotaEfetiva;
            // 6a Faixa  53,50%  21,50%  20,55%  4,45%   –
            this.irpj = this.impostoDas * 0.5350;
            this.csll = this.impostoDas * 0.2150;
            this.cofins = this.impostoDas * 0.2055;
            this.pis = this.impostoDas * 0.0445;
            this.iss = 0;

          }
          if (this.st === true) {
            this.impostoDas = this.impostoDas - this.iss;
            this.iss = 0;
          }
          break;
        }
      case '5':
        {
          if (this.fp12 === 0) {
            this.fatorR = 0;
          } else {
            this.fatorR = this.fp12 / this.rbt12;
          }

          if (this.fatorR >= 0.28) {
            if (this.rbt12 >= 0 && this.rbt12 <= 180000) {
              this.aliquotaNominal = 0.06;
              this.parcelaADeduzir = 0;
              if (this.rbt12 === 0 || this.rbt12 === undefined) {
              this.aliquotaEfetiva = this.aliquotaNominal;
            } else {
              this.aliquotaEfetiva = (((this.rbt12 * this.aliquotaNominal) - this.parcelaADeduzir) / this.rbt12);
            }
              this.impostoDas = this.faturamentoMes * this.aliquotaEfetiva;
              this.irpj = this.impostoDas * 0.04;
              this.csll = this.impostoDas * 0.035;
              this.cofins = this.impostoDas * 0.1282;
              this.pis = this.impostoDas * 0.0278;
              this.cpp = this.impostoDas * 0.4340;
              this.iss = this.impostoDas * 0.3550;

            } else if (this.rbt12 >= 180000.01 && this.rbt12 <= 360000) {
              this.aliquotaNominal = 0.1128;
              this.parcelaADeduzir = 9360;
              this.aliquotaEfetiva = (((this.rbt12 * this.aliquotaNominal) - this.parcelaADeduzir) / this.rbt12);
              this.impostoDas = this.faturamentoMes * this.aliquotaEfetiva;
              this.irpj = this.impostoDas * 0.04;
              this.csll = this.impostoDas * 0.035;
              this.cofins = this.impostoDas * 0.1405;
              this.pis = this.impostoDas * 0.0305;
              this.cpp = this.impostoDas * 0.4340;
              this.iss = this.impostoDas * 0.3200;
            } else if (this.rbt12 >= 360000.01 && this.rbt12 <= 720000) {
              this.aliquotaNominal = 0.1350;
              this.parcelaADeduzir = 17640;
              this.aliquotaEfetiva = (((this.rbt12 * this.aliquotaNominal) - this.parcelaADeduzir) / this.rbt12);
              this.impostoDas = this.faturamentoMes * this.aliquotaEfetiva;
              this.irpj = this.impostoDas * 0.04;
              this.csll = this.impostoDas * 0.035;
              this.cofins = this.impostoDas * 0.1364;
              this.pis = this.impostoDas * 0.0296;
              this.cpp = this.impostoDas * 0.4340;
              this.iss = this.impostoDas * 0.3250;
            } else if (this.rbt12 >= 720000.01 && this.rbt12 <= 1800000) {
              this.aliquotaNominal = 0.16;
              this.parcelaADeduzir = 35640;
              this.aliquotaEfetiva = (((this.rbt12 * this.aliquotaNominal) - this.parcelaADeduzir) / this.rbt12);
              this.impostoDas = this.faturamentoMes * this.aliquotaEfetiva;
              this.irpj = this.impostoDas * 0.04;
              this.csll = this.impostoDas * 0.035;
              this.cofins = this.impostoDas * 0.1364;
              this.pis = this.impostoDas * 0.0296;
              this.cpp = this.impostoDas * 0.4340;
              this.iss = this.impostoDas * 0.3250;
            } else if (this.rbt12 >= 1800000.01 && this.rbt12 <= 3600000) {
              this.aliquotaNominal = 0.21;
              this.parcelaADeduzir = 125640;
              this.aliquotaEfetiva = (((this.rbt12 * this.aliquotaNominal) - this.parcelaADeduzir) / this.rbt12);
              this.impostoDas = this.faturamentoMes * this.aliquotaEfetiva;
              this.irpj = this.impostoDas * 0.04;
              this.csll = this.impostoDas * 0.035;
              this.cofins = this.impostoDas * 0.1282;
              this.pis = this.impostoDas * 0.0278;
              this.cpp = this.impostoDas * 0.4340;
              this.iss = this.impostoDas * 0.3350;

              if (this.aliquotaEfetiva > 0.1492537) {
                /*IRPJ                CSLL              Cofins                PIS/Pasep               CPP                 ISS
                 * (Alíquota efetiva –5%) x6,02%  (Alíquota efetiva –5%) x 5,26%  (Alíquota efetiva –5%) x 19,28%
                 * (Alíquota efetiva – 5%) x 4,18%   (Alíquota efetiva –5%) x 65,26%   Percentual de ISS fixo em 5%
                 * */
                this.irpj = ((this.aliquotaEfetiva - 0.05) * 0.0602) * this.impostoDas;
                this.csll = ((this.aliquotaEfetiva - 0.05) * 0.0526) * this.impostoDas;
                this.cofins = ((this.aliquotaEfetiva - 0.05) * 0.1928) * this.impostoDas;
                this.pis = ((this.aliquotaEfetiva - 0.05) * 0.0418) * this.impostoDas;
                this.cpp = ((this.aliquotaEfetiva - 0.05) * 0.6526) * this.impostoDas;
                this.iss = 0.05 * this.impostoDas;
              }
            } else if (this.rbt12 >= 3600000.01 && this.rbt12 <= 4800000) {
              this.aliquotaNominal = 0.33;
              this.parcelaADeduzir = 648000;
              this.aliquotaEfetiva = (((this.rbt12 * this.aliquotaNominal) - this.parcelaADeduzir) / this.rbt12);
              this.impostoDas = this.faturamentoMes * this.aliquotaEfetiva;
              this.irpj = this.impostoDas * 0.35;
              this.csll = this.impostoDas * 0.15;
              this.cofins = this.impostoDas * 0.1603;
              this.pis = this.impostoDas * 0.0347;
              this.cpp = this.impostoDas * 0.3050;
              this.iss = 0;
            }
          } else {
            /* 15,50%  R$-
             * 18,00%  R$4.500,00
             * 19,50%  R$9.900,00
             * 20,50%  R$17.100,00
             * 23,00%  R$62.100,00
             * 30,50%  R$540.000,00
             *
             */
            /*
              * Faixa   IRPJ  CSLL  Cofins  PIS/Pasep   CPP   ISS
              * 1a Faixa  25,00%  15,00%  14,10%  3,05%     28,85%  14,00%
              * 2a Faixa  23,00%  15,00%  14,10%  3,05%     27,85%  17,00%
              * 3a Faixa  24,00%  15,00%  14,92%  3,23%     23,85%  19,00%
              * 4a Faixa  21,00%  15,00%  15,74%  3,41%     23,85%  21,00%
              * 5a Faixa  23,00%  12,50%  14,10%  3,05%     23,85%  23,50%
              * 6a Faixa  35,00%  15,50%  16,44%  3,56%     29,50%  –
            */
            if (this.rbt12 >= 0 && this.rbt12 <= 180000) {
              this.aliquotaNominal = 0.15;
              this.parcelaADeduzir = 0;
              if (this.rbt12 === 0 || this.rbt12 === undefined) {
              this.aliquotaEfetiva = this.aliquotaNominal;
            } else {
              this.aliquotaEfetiva = (((this.rbt12 * this.aliquotaNominal) - this.parcelaADeduzir) / this.rbt12);
            }
              this.impostoDas = this.faturamentoMes * this.aliquotaEfetiva;
              this.irpj = this.impostoDas * 0.25;
              this.csll = this.impostoDas * 0.15;
              this.cofins = this.impostoDas * 0.1410;
              this.pis = this.impostoDas * 0.0305;
              this.cpp = this.impostoDas * 0.2885;
              this.iss = this.impostoDas * 0.1400;
            } else if (this.rbt12 >= 180000.01 && this.rbt12 <= 360000) {
              this.aliquotaNominal = 0.18;
              this.parcelaADeduzir = 4500;
              this.aliquotaEfetiva = (((this.rbt12 * this.aliquotaNominal) - this.parcelaADeduzir) / this.rbt12);
              this.impostoDas = this.faturamentoMes * this.aliquotaEfetiva;
              this.irpj = this.impostoDas * 0.23;
              this.csll = this.impostoDas * 0.15;
              this.cofins = this.impostoDas * 0.1410;
              this.pis = this.impostoDas * 0.0305;
              this.cpp = this.impostoDas * 0.2785;
              this.iss = this.impostoDas * 0.1700;
            } else if (this.rbt12 >= 360000.01 && this.rbt12 <= 720000) {
              this.aliquotaNominal = 0.195;
              this.parcelaADeduzir = 9900;
              this.aliquotaEfetiva = (((this.rbt12 * this.aliquotaNominal) - this.parcelaADeduzir) / this.rbt12);
              this.impostoDas = this.faturamentoMes * this.aliquotaEfetiva;
              // 3a Faixa  24,00%  15,00%  14,92%  3,23%     23,85%  19,00%
              this.irpj = this.impostoDas * 0.24;
              this.csll = this.impostoDas * 0.15;
              this.cofins = this.impostoDas * 0.1492;
              this.pis = this.impostoDas * 0.0323;
              this.cpp = this.impostoDas * 0.2385;
              this.iss = this.impostoDas * 0.19;
            } else if (this.rbt12 >= 720000.01 && this.rbt12 <= 1800000) {
              this.aliquotaNominal = 0.205;
              this.parcelaADeduzir = 17100;
              this.aliquotaEfetiva = (((this.rbt12 * this.aliquotaNominal) - this.parcelaADeduzir) / this.rbt12);
              this.impostoDas = this.faturamentoMes * this.aliquotaEfetiva;
              // 4a Faixa  21,00%  15,00%  15,74%  3,41%     23,85%  21,00%
              this.irpj = this.impostoDas * 0.21;
              this.csll = this.impostoDas * 0.15;
              this.cofins = this.impostoDas * 0.1574;
              this.pis = this.impostoDas * 0.0341;
              this.cpp = this.impostoDas * 0.2385;
              this.iss = this.impostoDas * 0.21;
            } else if (this.rbt12 >= 1800000.01 && this.rbt12 <= 3600000) {
              this.aliquotaNominal = 0.23;
              this.parcelaADeduzir = 62100;
              this.aliquotaEfetiva = (((this.rbt12 * this.aliquotaNominal) - this.parcelaADeduzir) / this.rbt12);
              this.impostoDas = this.faturamentoMes * this.aliquotaEfetiva;
              // 5a Faixa  23,00%  12,50%  14,10%  3,05%     23,85%  23,50%
              this.irpj = this.impostoDas * 0.23;
              this.csll = this.impostoDas * 0.1250;
              this.cofins = this.impostoDas * 0.1410;
              this.pis = this.impostoDas * 0.0305;
              this.cpp = this.impostoDas * 0.2385;
              this.iss = this.impostoDas * 0.235;
            } else if (this.rbt12 >= 3600000.01 && this.rbt12 <= 4800000) {
              this.aliquotaNominal = 0.305;
              this.parcelaADeduzir = 540000;
              this.aliquotaEfetiva = (((this.rbt12 * this.aliquotaNominal) - this.parcelaADeduzir) / this.rbt12);
              this.impostoDas = this.faturamentoMes * this.aliquotaEfetiva;
              // 6a Faixa  35,00%  15,50%  16,44%  3,56%     29,50%  –
              this.irpj = this.impostoDas * 0.35;
              this.csll = this.impostoDas * 0.1550;
              this.cofins = this.impostoDas * 0.1644;
              this.pis = this.impostoDas * 0.0356;
              this.cpp = this.impostoDas * 0.2950;
              this.iss = 0;
            }

          }
          if (this.st === true) {
              this.impostoDas = this.impostoDas - this.iss;
              this.iss = 0;
            }
          break;
        }
      default:
        {
          break;
        }
    }
  }
  ngOnInit() {}
}

